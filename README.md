# open-frame-scan

<img src="img/frame.jpg" width=70%></img>

This device is an open-source approach to scanning flat surfaces. The frame is simply placed on top of the surface to scan, and a computer vision software analyzes the picture to extract a rectified picture with real dimensions (pixels/unit).

<img src="img/comparison.jpg" width=70%></img>

This project is inspired by the [Shaper Trace](https://www.shapertools.com/en-us/trace), but offers an open source interface and lets users scan color images, not just vectors.

## designs

<img src="img/sizes.png" width=50%></img>

There are three different sizes:

|            | **Outer dimensions** | **Content size** |
|------------|----------------------|------------------|
| **Small**  | 170x250mm            | 130x210mm        |
| **Medium** | 240x320mm            | 180x260mm        |
| **Large**  | 300x380mm            | 240x320mm        |

The smallest one can be printed on an A4 page (or letter size), and the largest one can fully scan a page of that size. The medium size is an in-between, with a similar form factor as the [Shaper Trace](https://www.shapertools.com/en-us/trace).

## Fabrication

### Vinyl cutting

If you have access to digital fabrication tools, the most straightforward way to build your frame is to laser cut the shape, then vinyl cut the features to be added onto it.

The first step is to weed the vinyl in place:

<img src="img/vinyl1.jpg" width=50%></img>

The result is then stuck onto transfer tape and laid flat:

<img src="img/vinyl2.jpg" width=50%></img>

After joining to the frame, the transfer is peeled off, revealing the finished frame:

<img src="img/vinyl3.jpg" width=50%></img>

### Inkjet printing

Alternatively, you can print the design of the frame using an inkjet printer, then glue it to a thin piece of cardboard:

<img src="img/cardboard.jpg" width=50%></img>

The small frame is designed to be printable on an A4 page (or letter), and the medium frame fits on an A3 page.

## Results

### Grid scan

To quantify the accuracy of the scanning, a 25mm calibration grid was built:

<img src="img/grid.jpg" width=70%></img>

Here is the image used for analysis, showing the detected features (aruco markers in blue, corners in red):

<img src="img/features.jpg" width=70%></img>

Here is the extracted image:

<img src="img/result.jpg" width=60%></img>

The grid feature points are automatically extracted:

<img src="img/grid_features.jpg" width=60%></img>

Finally, the distance between neighboring grid points is measured, revealing the accuracy of the scanning process. Here is a comparison of all frames, including the Shaper Trace, showing improved accuracy and less variance:

<img src="img/plot.png" width=70%></img>

Here are numerical results:

|                  | mean [mm] | stdev [mm] |
|------------------|-----------|------------|
| **small1**       | 25.098    | 0.126      |
| **small2**       | 25.017    | 0.102      |
| **medium1**      | 25.048    | 0.123      |
| **medium2**      | 25.056    | 0.123      |
| **large**        | 25.086    | 0.112      |
| **Shaper Trace** | 24.888    | 0.201      |

### Resilience to fabrication error

To test the resilience of the scanning to process to fabrication errors, a distorted frame was designed and built. Its feature points are deviated horizontally and vertically with a standard deviation of 1mm.

<img src="img/distorted_zoom.jpg" width=70%></img>

Thanks to the least squares optimization when retrieving the perspective transform, this error is successfully dampened. The results are shown in the table above, under the name **medium2**; as seen, the precision is the same as **medium1**, which was built with a tolerance of <0.4mm .
